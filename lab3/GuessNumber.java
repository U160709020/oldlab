import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in); // Creates an object to read user input
		Random rand = new Random(); // Creates an object from Random class
		int number = rand.nextInt(100); // generates a number between 0 and 99

		// System.out.print("Can you guess it: ");
		
		int a = 1;
		int guess = 1111111;
		int attemps=0;

		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		while (a == 1 && guess != -1) {

			System.out.println("If you want to exit the game press -1");
			System.out.println("Can you guess it: ");

			guess = reader.nextInt();
			attemps+=1;

			if (guess == number) {
				System.out.print("Congratilations!! You won after "+attemps+" attemps.");

				a += 1;

			} else {
				System.out.println("Sorry , try again :");
				
				if (guess < number) {
					System.out.println("Number is greater than your guess");
				} else {
					System.out.println("Number is less than your guess");
				}

			}
		}

	}

}
