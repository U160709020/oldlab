public class Main {

	public static void main(String[] args) {
		Point p = new Point(3,5);
		System.out.println("x="+ p.xCoord + " y="+ p.yCoord);
		Rectangle r = new Rectangle(6,8,p);
		r.topLeft = p;
		System.out.println(r.area());
		System.out.println(r.perimeter());
		Point[] corners = r.corners();
		for(int i=0; i<corners.length; i++) {
			System.out.println("x="+ corners[i].xCoord + " y="+ corners[i].yCoord);
	}
		
		Circle c = new Circle(8,new Point(5,5));
		System.out.println(c.area());
		System.out.println(c.perimeter());
		System.out.println(c.intersect(new Circle(7,new Point(7,5))));
		
}
}
