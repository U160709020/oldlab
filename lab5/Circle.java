

public class Circle {
	int radius;
	
	
	public double area() {
		return 3.14 * radius * radius;
		
	}

	public double perimeter() {
		return radius;
	
	
}
}