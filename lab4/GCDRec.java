  public class GCDRec {

    public static void main(String[] args) {
       int n1 = Integer.parseInt(args[0]);
       int i = 1;
       int n2 = Integer.parseInt(args[i]);
        int hcf = hcf(n1, n2);

        System.out.printf("G.C.D of %d and %d is %d.", n1, n2, hcf);
    }

    public static int hcf(int n1, int n2)
    {
        if (n2 != 0)
            return hcf(n2, n1 % n2);
        else
            return n1;
    }
}



