public class GCDLoop{
  public static void main(String[] args){
    int a = Math.max(Integer.parseInt(args[0]),Integer.parseInt(args[1]));    
    int b = Math.min(Integer.parseInt(args[0]),Integer.parseInt(args[1]));     
    for (int z = a % b; z != 0; z = a % b){
      a = b;
      b = z;
    }
    System.out.println(b);
  }
}
