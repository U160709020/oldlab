public class FindSecondMax {

    public static void main(String[] args) {
        int max=Integer.MIN_VALUE;
        int sMax=Integer.MIN_VALUE;
        for (int i = 0; i < args.length; i++) {
            int number=Integer.parseInt(args[i]);
            if (max<number) {
                sMax=max;
                max=number;
            }
            else if (number<max && number>sMax) {
                sMax=number;
            }
        }
        if (sMax==Integer.MIN_VALUE) {
            System.out.println("There is no Second Max");
        }
        else{
            System.out.println(sMax);
        }
    }
}
